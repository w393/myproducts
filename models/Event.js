const mongoose = require('mongoose')
const { Schema } = mongoose
const eventSchema = Schema({
  startDate: Date,
  endDate: Date,
  title: String,
  content: String,
  class: String
}, {
  timestamps: true
})

module.exports = mongoose.model('Event', eventSchema)
