const mongoose = require('mongoose')
const Event = require('../models/Event')
mongoose.connect('mongodb://localhost:27017/example')
async function clearEvent () {
  await Event.deleteMany({})
}
async function main () {
  await clearEvent()
  await Event.insertMany([
    {
      title: 'title1', content: 'C1', startDate: new Date('2022-03-03 08:00'), endDate: new Date('2022-03-03 09:00'), class: 'a'
    },
    {
      title: 'title2', content: 'C2', startDate: new Date('2022-03-30 08:00'), endDate: new Date('2022-03-30 09:00'), class: 'b'
    },
    {
      title: 'title3', content: 'C3', startDate: new Date('2022-03-31 08:00'), endDate: new Date('2022-03-31 09:00'), class: 'c'
    }
  ])
  Event.find({})
}

main().then(function () {
  console.log('Finish')
})
