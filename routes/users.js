const express = require('express')
const router = express.Router()
const User = require('../models/User')

router.get('/', async (req, res, next) => {
  try {
    const users = await User.find({}).exec()
    res.status(200).json(users)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
})

router.get('/:id', async (req, res, next) => {
  const id = req.params.id
  try {
    const user = await User.findById(id).exec()
    if (user === null) {
      return res.status(404).send({
        message: 'User not found'
      })
    }
    res.json(user)
  } catch (err) {
    return res.status(404).send({
      message: err.message
    })
  }
})

router.post('/', async (req, res, next) => {
  const newUser = new User({
    username: req.body.username,
    password: req.body.password,
    roles: req.body.roles
  })
  try {
    await newUser.save()
    res.status(201).json(newUser)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
})

router.put('/:id', async (req, res, next) => {
  const pId = req.params.id
  try {
    const user = await User.findById(pId)
    user.username = req.body.username
    user.password = req.body.password
    user.roles = req.body.roles
    await user.save()
    return res.status(200).json(user)
  } catch (err) {
    return res.status(404).send({
      message: err.message
    })
  }
})

router.delete('/:id', async (req, res, next) => {
  const pId = req.params.id
  try {
    await User.findByIdAndDelete(pId)
    return res.status(200).send()
  } catch (err) {
    return res.status(404).send({
      message: err.message
    })
  }
})

module.exports = router
