const express = require('express')
const router = express.Router()
const Event = require('../models/Event')

router.get('/', async (req, res, next) => {
  try {
    const startDate = req.query.startDate
    const endDate = req.query.endDate
    const events = await Event.find({
      $or: [{ startDate: { $gte: startDate, $lt: endDate } },
        { endDate: { $gte: startDate, $lt: endDate } }]
    }).exec()
    res.status(200).json(events)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
})

module.exports = router
