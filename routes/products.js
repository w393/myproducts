const express = require('express')
const router = express.Router()
const Product = require('../models/Product')

router.get('/', async (req, res, next) => {
  try {
    const products = await Product.find({}).exec()
    res.status(200).json(products)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
})

router.get('/:id', async (req, res, next) => {
  const id = req.params.id
  try {
    const product = await Product.findById(id).exec()
    if (product === null) {
      return res.status(404).send({
        message: 'Product not found'
      })
    }
    res.json(product)
  } catch (err) {
    return res.status(404).send({
      message: err.message
    })
  }
})

router.post('/', async (req, res, next) => {
  const newProduct = new Product({
    name: req.body.name,
    price: parseFloat(req.body.price)
  })
  try {
    await newProduct.save()
    res.status(201).json(newProduct)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
})

router.put('/:id', async (req, res, next) => {
  const pId = req.params.id
  try {
    const product = await Product.findById(pId)
    product.name = req.body.name
    product.price = parseFloat(req.body.price)
    await product.save()
    return res.status(200).json(product)
  } catch (err) {
    return res.status(404).send({
      message: err.message
    })
  }
})

router.delete('/:id', async (req, res, next) => {
  const pId = req.params.id
  try {
    await Product.findByIdAndDelete(pId)
    return res.status(200).send()
  } catch (err) {
    return res.status(404).send({
      message: err.message
    })
  }
})

module.exports = router
